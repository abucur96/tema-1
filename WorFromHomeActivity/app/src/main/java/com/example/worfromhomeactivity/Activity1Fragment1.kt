package com.example.worfromhomeactivity

//import android.R;
import com.example.worfromhomeactivity.R
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.Fragment


class Activity1Fragment1 : Fragment() {

var button: Button? = null
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_activity11, container, false)

        button = view.findViewById(R.id.btn_go_to_second_activity) as Button
        button?.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                startActivity(Intent(activity, secondActivity::class.java))
            }
        })

        return view
    }



}
