package com.example.worfromhomeactivity

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.FragmentTransaction
import com.example.worfromhomeactivity.R


class Activity2Fragment1 : Fragment() {

    private var button:Button? = null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_activity21, container, false)

        button = view.findViewById(R.id.btn_show_fragment2A2) as Button
        button?.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                //startActivity(Intent(activity, MainActivity::class.java))
                val fragment2 =  Activity2Fragment2()
                val fragmentTransaction: FragmentTransaction? = fragmentManager!!.beginTransaction()
                if(fragmentTransaction != null)
                {
                    fragmentTransaction.replace(R.id.FraameContainer, fragment2)
                    fragmentTransaction.commit()
                }
            }
        })

        return view
    }


}
