package com.example.worfromhomeactivity

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction


class Activity2Fragment2 : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view:View = inflater.inflate(R.layout.fragment_activity22, container, false)

        val buttonReplaceF2WithF3 =  view.findViewById(R.id.btn_replace_f2_with_f3) as Button
        val buttonRemoveFragment1 =  view.findViewById(R.id.btn_remove_fragment1) as Button
        val buttonCloseActivity =  view.findViewById(R.id.btn_close_activity) as Button

        buttonReplaceF2WithF3.setOnClickListener(clickListener)
        buttonRemoveFragment1.setOnClickListener(clickListener)
        buttonCloseActivity.setOnClickListener(clickListener)


        return view
    }

    private val clickListener = View.OnClickListener { view ->

        when(view.id)
        {
            R.id.btn_replace_f2_with_f3 -> replaceF2WithF3()
            R.id.btn_remove_fragment1 -> removeF1()
            R.id.btn_close_activity -> closeActivity()
        }
    }

    private fun replaceF2WithF3()
    {
        val fragment3 =  Activity2Fragment3()
        val fragmentTransaction: FragmentTransaction? = fragmentManager!!.beginTransaction()
        if(fragmentTransaction != null)
        {
            fragmentTransaction.replace(R.id.FraameContainer, fragment3)
            fragmentTransaction.addToBackStack(null)
            fragmentTransaction.commit()
        }
    }

    private fun removeF1()
    {
        //aici nu am reusit sa rezolv o problema...imi spune ca acest fragment apartine altui Fragment Manager

        val fragmentTransaction: FragmentTransaction? = childFragmentManager.beginTransaction()
        //fragmentManager!!.beginTransaction()
        if(fragmentTransaction != null)
        {
            val fragment2 =  activity!!.supportFragmentManager.findFragmentById(R.id.Activity2Fragment1)
            if (fragment2 != null) {
                fragmentTransaction.remove(fragment2)
                fragmentTransaction.commit()
            }

        }
    }

    private fun closeActivity()
    {
        activity!!.finish()
    }

}




